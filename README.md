# CCD Performance Practice Environment
*We reccomend you use the VSCode and remote container environment following the steps below.*
*If you are unable to install docker, we have provided [alternative methods](https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/basic-dev-prac-env/-/wikis/Alternative-Setup-Methods-(outside-of-the-docker-environment))*

#### 0. Ensure you have installed the prerequisite software

- [Ubuntu](https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/basic-dev-prac-env/-/wikis/Ubuntu-prerequisite-setup)
- [Windows 10](https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/basic-dev-prac-env/-/wikis/Windows-10-prerequisite-setup)
- [Mac](https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/basic-dev-prac-env/-/wikis/Mac-prerequisite-setup)

#### 1. Initial Setup
```sh
git clone https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/basic-dev-prac-env && \
cd basic-dev-prac-env && code .
```

#### 2. Follow On Steps
- Once the project is open a notification toast will appear in the bottom right corner of the VSCode window. Click "Reopen in Container"
  - Alternatively you can click the green button at the bottom left of the VSCode window and choose the same option
- Wait Patiently... It may take 3-8 minutes depending on your hardware and network connection to pull and stage the container


## Generating a Practice test
Once the environement is all set up and ready to go you will need to generate an initial practice test. Run the following command in the VSCode console (cntl+shift+`).
```sh
./createPracticeTest.py
```
WARNING: All previously worked on questions will be deleted, so save any work you do not want to lose.


## Building/Compiling/Running Questions
Follow the [compile-instructions.md](compile-instructions.md) once your environment is staged and ready to go. While in VSCode and focusing on the file, press `ctrl+shift+v`. This will open up a new tab with the rendered markdown.

**NOTE: Networking questions will not include the `server.py` file to verify your work**


## VSCode Tasks
Click the button labeled "Tasks" in the bottom blue bar to view tasks to execute. The task heirarchy is the following. These tasks are only for C Programming.
```sh
└─ Compile C
│   └─ Build C
└─ Clean 
```

### Features
- Lightweight and opensource C and Python development environment
- C build and compilation with CMake
- Debugging for C and Python
  - gdb debugging for C through VSCode GUI
  - Breakpoints, step through, etc.
  - Variable and Call Stack analysis
- Docker
  - Easy to deploy containerized dev environment
  - Wraps all extensions, libraries, and tools
  - It can be personalize

### Sources
- https://cmake.org/cmake/help/v3.13/
- https://code.visualstudio.com/docs/remote/containers
- https://code.visualstudio.com/docs/remote/containers-advanced
- https://code.visualstudio.com/docs/remote/wsl

# How to Contribute
As this environment is on the path to be used for the official Basic-Dev performance eval, there are many ways with different levels of involvement one can have to make a contribution.
1. If you encounter a problem anywhere, create an [issue ticket](https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/basic-dev-prac-env/-/issues/new) on this repo. This will highlight the problem to the community and act as a focalpoint to track progress as our team resolves it.
2. Create an [issue ticket](https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/basic-dev-prac-env/-/issues/new), create a branch from the ticket, fix the problem, and submit a merge request. Our team will analyze the changes and merge it into the project. 

Our team is small and working on many different projects. Your contribution will help tremendously.
