#!/bin/bash

# Check if python3 is installed.
which -s python3.8
if [[ $? != 0 ]] ; then
    echo "python3.8 is not installed"
fi

# Check if pip3 is installed.
which -s pip3
if [[ $? != 0 ]] ; then
    echo "pip3 is not installed"
fi

# Check if gcc is installed.
which -s gcc
if [[ $? != 0 ]] ; then
    echo "gcc is not installed"
fi

# Check if cmake module is installed.
pip list | grep -F cmake
if [[ $? != 0 ]] ; then
    echo "cmake module is not installed"
fi

# Check if pylint module is installed.
pip list | grep -F pylint
if [[ $? != 0 ]] ; then
    echo "pylint module is not installed"
fi

# Check if xmlrunner module is installed.
pip list | grep -F xmlrunner
if [[ $? != 0 ]] ; then
    echo "xmlrunner module is not installed"
fi
